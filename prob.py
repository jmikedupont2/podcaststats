import sys
import re
import datetime
from datetime import timedelta
def main(filename):
    with open(filename) as fi:
        data = fi.read()
        dates = sorted([datetime.datetime.strptime(d, '%a, %d %b %Y %H:%M:%S +0000')
                        for d in re.findall(r'\<pubDate\>(?P<date>[^\<]+)\<\/pubDate\>',data)])
        first = dates[0]; last = dates[-1]
        alltime = last - first
        count = len(dates)
        days = alltime.days

        for dow in range(0,7):
            count = len([date for date in dates if date.weekday() == dow])
            print("alltime", dow, count/days)

        print("all time",count/days)
        for sample in(7, 30, 90, 365, 365 *2):
            start = last - timedelta(days=sample)
            count = len([date for date in dates if date >= start])
            print("sample", sample,count/sample)
            
            for dow in range(0,7):
                start = last - timedelta(days=sample)
                count = len([date for date in dates if date >= start and date.weekday() == dow])
                print("sample", sample,dow, count/sample)
            
if __name__ == '__main__':    
    main(sys.argv[1])
    
"""
wget https://feeds.simplecast.com/54nAGcIl
python3 prob.py ./54nAGcIl 

all time 0.7679296346414073
sample 7 1.1428571428571428
sample 30 0.9
sample 90 0.8111111111111111
sample 365 0.8958904109589041
sample 730 0.8356164383561644

"""
