import sys
import re
import datetime
from datetime import timedelta
from datetime import date
import calendar

from dateutil import parser

def main(filename):
    with open(filename) as fi:
        data = fi.read()
        
        dates = sorted([parser.parse(d) # , '%a, %d %b %Y %H:%M:%S %z'
                        for d in re.findall(r'\<pubDate\>(?P<date>[^\<]+)\<\/pubDate\>',data)])
        first = dates[0]; last = dates[-1]
        alltime = last - first
        count = len(dates)
        days = alltime.days
                
        for sample in(7, 30, 90, 365, days):
            start = last - timedelta(days=sample)
            count = len([date for date in dates if date >= start])
            print("sample", sample,count/sample)

            print ("dow", sample)
            for dow in range(0,7):
                name = calendar.day_name[dow]
                start = last - timedelta(days=sample)
                count = len([date for date in dates if date >= start and date.weekday() == dow])
                print("\tsample dow", sample,dow,name, count/sample)

            print ( "hours", sample)
            for hr in range(0,24):
                count = len([date for date in dates if date >= start and date.hour == hr])
                print("\tsample hour", sample, hr, count/sample)

            print ("dow_hour", sample)
            for dow in range(0,7):
                name = calendar.day_name[dow]
                start = last - timedelta(days=sample)
                for hr in range(0,24):
                    count = len([date for date in dates if date >= start and date.hour == hr and date.weekday() == dow])
                    print("\tdow sample hour", sample, dow, name, hr, count/sample)

if __name__ == '__main__':    
    main(sys.argv[1])

"""

python3 stats.py ./54nAGcIl 

alltime 0 0.13058186738836267
alltime 1 0.14411366711772666
alltime 2 0.14682002706359945
alltime 3 0.14682002706359945
alltime 4 0.14817320703653586
alltime 5 0.02165087956698241
alltime 6 0.029769959404600813
all time 0.029769959404600813

sample 7 1.1428571428571428

sample 7 0 0.14285714285714285
sample 7 1 0.14285714285714285
sample 7 2 0.14285714285714285
sample 7 3 0.42857142857142855
sample 7 4 0.14285714285714285
sample 7 5 0.0
sample 7 6 0.14285714285714285

sample 30 0.9

sample 30 0 0.1
sample 30 1 0.16666666666666666
sample 30 2 0.16666666666666666
sample 30 3 0.2
sample 30 4 0.13333333333333333
sample 30 5 0.0
sample 30 6 0.13333333333333333

sample 90 0.8111111111111111

sample 90 0 0.13333333333333333
sample 90 1 0.14444444444444443
sample 90 2 0.14444444444444443
sample 90 3 0.14444444444444443
sample 90 4 0.1111111111111111
sample 90 5 0.011111111111111112
sample 90 6 0.12222222222222222

sample 365 0.8958904109589041

sample 365 0 0.13424657534246576
sample 365 1 0.1506849315068493
sample 365 2 0.15342465753424658
sample 365 3 0.1506849315068493
sample 365 4 0.15616438356164383
sample 365 5 0.038356164383561646
sample 365 6 0.11232876712328767

sample 730 0.8356164383561644

sample 730 0 0.13287671232876713
sample 730 1 0.15205479452054796
sample 730 2 0.15205479452054796
sample 730 3 0.15616438356164383
sample 730 4 0.1547945205479452
sample 730 5 0.028767123287671233
sample 730 6 0.0589041095890411

"""
