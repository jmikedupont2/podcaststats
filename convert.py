#!/usr/bin/python3
#convert html table to opml
from bs4 import BeautifulSoup

def quote(x):
    y = x.replace("&","&amp;")
    return y.replace("\"","")
OPML_START = """<?xml version="1.0" encoding="utf-8"?>
<opml version="2.0">
<head>
<title>Daily Podcasts</title>
</head>
<body>
"""

OPML_END = """
</body>
</opml>"""

OPML_OUTLINE_FEED = '<outline text="%(text)s" title="%(title)s" type="rss" xmlUrl="%(xmlUrl)s" htmlUrl="%(htmlUrl)s" />'

print (OPML_START)
with open ("example/report.html") as fi:
    soup = BeautifulSoup(fi.read(), features="lxml")
    data = []
    table = soup.find('table')
    rows = table.find_all('tr')
    cols = []
    
    header =  rows[0]
    cols = header.find_all('th')
    cols = [ele.text.strip() for ele in cols]
    #print (cols)
    
    for row in rows[1:]:
        d = {}
        cols2 = row.find_all('td')
        cols2 = [ele.text.strip() for ele in cols2]
        for i,c in enumerate(cols):
            d[c] = cols2[i]
        #print(d)
        print (OPML_OUTLINE_FEED % {
            'title': quote(d['title']),
            'text': " ".join([d['ratio'], quote(d['title']) ]),
            'xmlUrl': quote(d['url']),
            'htmlUrl': quote(d['link'])
        })
print (OPML_END)
