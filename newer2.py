#head podcasts-2021-02-01.csv  | cut -d, -f 2,5
import datetime
cutoff = 1612221327.0
first = cutoff - (60 * 60 * 24 * 2) # two days
import re
import csv
import requests
import os.path

with open("podcasts-2021-02-01-update.csv") as fi:
    csv1 = csv.DictReader(fi)
    for l in csv1:
        ll = l['language']
        if 'en' in ll:
            url = l['url']
            if 'church' not in url and 'sermon' not in url:
                id1 = l['id']
                fn = id1+ ".rss"
                if (not os.path.exists(fn)):
                    with open(fn,"w") as fo:
                        try:
                            r = requests.get(url, timeout=5.0)
                            print(url)
                            fo.write(r.text)
                        except Exception as e:
                            print(e, url)
                        
