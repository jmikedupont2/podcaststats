import datetime
import feedparser
from datetime import timedelta
import calendar
import re
import csv
import requests
import os.path
from dateutil import parser
from dateutil.tz import gettz

tzinfos = {
    "PST": gettz("America/Los Angeles"),
    "CST" : gettz("America/Chicago"),
    "CSD" : gettz("America/Chicago")
}
def read_rss(fname, row):
    with open(fname) as fi:
        data = fi.read()

        dates = sorted([parser.parse(d,  tzinfos=tzinfos ) # , '%a, %d %b %Y %H:%M:%S %z'
                        for d in re.findall(r'\<pubDate\>(?P<date>[^\<]+)\<\/pubDate\>',data)])

        #dates = sorted([datetime.datetime.strptime(d, '%a, %d %b %Y %H:%M:%S +0000')
        #                for d in re.findall(r'\<pubDate\>(?P<date>[^\<]+)\<\/pubDate\>',data)])
        if len(dates) == 0:
            #print(fname,"empty")
            return
        first = dates[0]; last = dates[-1]
        alltime = last - first
        count = len(dates)
        days = alltime.days
                
        for sample in(30,):
            start = last - timedelta(days=sample)
            count = len([date for date in dates if date >= start])
            ratio = count/sample
            
            for dow in range(0,7):
                dowcount = len([date for date in dates if date >= start and date.weekday() == dow])
                dowratio = count/sample
                if dowratio < 1/7:
                    #print("missing",dow,fname,dowratio)
                    return # knock out if missing a day

            if ratio >= 1:
                #now we can parse the rss and try and collect some data from it.
                d = feedparser.parse(data)
                title =  d['feed']['title']

                link = d.feed.link
                desc = d.feed.description
                # id,url,itunes_id,original_url,newest_item_pubdate,oldest_item_pubdate,language
                print("<tr>{}</tr>".format(
                    "".join(["<td>{}</td>".format(name) for name in (
                        row['id'],
                        row['itunes_id'],
                        ratio,
                        title,
                        "<a href={}>{}</a>".format( row['url'], row['url']),
                        "<a href={}>{}</a>".format( link, link ),
                        desc,
                        row['language']
                        )])))

print("<html><body><table>")
print("<tr>{}</tr>".format("".join(["<th>{}</th>".format(name) for name in ('id','itunes_id', "ratio", "title", 'url', "link","desc", 'language')])))

with open("podcasts-2021-02-01-update.csv") as fi:
    csv1 = csv.DictReader(fi)
    for l in csv1:
        ll = l['language']
        if 'en' in ll:
            url = l['url']
            if 'church' not in url and 'sermon' not in url:
                id1 = l['id']
                fn = id1+ ".rss"
                if (os.path.exists(fn)):
                    try: 
                        read_rss(fn, l)
                    except Exception as e:
                        #print (fn,e)
                        pass
print("</table></body></html>")
