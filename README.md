
Simple podcast states using rss
```
wget https://feeds.simplecast.com/54nAGcIl
python3 stats.py ./54nAGcIl  > example/thedaily.txt
```

The first script reads the dump from archive org and looks for records that are updated within 2 days of the latest episode
it has some hardcoded cutoffs in there that need some adjustment.

`newer.py`


Day of week and Hour of day stats based on simple timestamps for different intervals.

Collecting the rss feeds use this script
`python3 newer2.py`

Get the count of rss
`
ls -latr *.rss | wc
`
current count is : 28403, another 10 to process.

This report will look for at least 30 episodes in the last 30 days and at the episodes should not be lest than 1/7 for every days of the week,
so if they have zero % on sunday they are not daily. 

```
python3 ./newer3.py > report2.txt
```

Get the report: 
```
grep -v missing report2.txt | sort -n -k2
```

