#head podcasts-2021-02-01.csv  | cut -d, -f 2,5
import datetime
cutoff = 1612221327.0
first = cutoff - (60 * 60 * 24 * 2) # two days
import re
import csv

with open("podcasts-2021-02-01.csv") as fi:
    with open("podcasts-2021-02-01-update.csv","w") as fo:
        csv1 = csv.DictReader(fi)
        csv2 = csv.DictWriter(fo, csv1.fieldnames)
        csv2.writeheader()
        for l in csv1:
            dv = l['newest_item_pubdate']
            dv = float(dv + ".0")
            dt = datetime.datetime.fromtimestamp(dv)
        
            if dv > first:
                csv2.writerow(l)
                
#print (cutoff)
